package ap.self.q2;

import functions.MyUserInput;

public class printFilledDiamond {

	static int i, s, sp;
	static int x = MyUserInput.readNumber("Enter the half Height of Filled Diamond");

	public static void main(String[] args) {
		printTopHalf();
		printBottomHalf();
	}

	public static void printTopHalf() {

		for (i = 1; i <= x; i++) {
			// x represents the Half height of Diamond
			for (sp = 1; sp <= x - i; sp++) {
				System.out.print(" ");
			}
			
			for (s = 1; s <= i; s++) {
				System.out.print("/");

			}
			for (s = 1; s <= i; s++) {

				System.out.print("\\");
			}
			System.out.println("");
		}

	}

	public static void printBottomHalf() {

		for (i = 1; i <= x; i++) {
			
			for (sp = 1; sp<= i - 1; sp++) {
				System.out.print(" ");
			}

			for (s = 1; s <= (x + 1) - i; s++) {
				System.out.print("\\");

			}
			for (s = 1; s <= (x + 1) - i; s++) {

				System.out.print("/");
			}
			System.out.println("");
		}

	}
}
