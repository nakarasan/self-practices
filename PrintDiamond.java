package ap.self.q1;

import functions.MyUserInput;

public class PrintDiamond {

	static int i, s, sp;
	static int x = MyUserInput.readNumber("Enter the half Height of Diamond");

	public static void main(String[] args) {

		printTopHalf();
		printBottomHalf();

	}

	
	public static void printTopHalf() {

		for (i = 1; i <= x; i++) {
		
			for (sp = 1; sp<= x - i; sp++) {
				System.out.print(" ");
			}
			System.out.print("/");

			for (sp = 1; sp <= i * 2 - 2; sp++) {
				System.out.print(" ");

			}
			System.out.println("\\");
		}

	}

	
	public static void printBottomHalf() {

		for (i = 1; i <= x; i++) {
			for (sp = 1; sp <= i - 1; sp++) {
				System.out.print(" ");
			}
			System.out.print("\\");

			for (sp = 1; sp <= (x * 2) - (i * 2); sp++) {
				System.out.print(" ");

			}
			System.out.println("/");
		}

	}

}

// output
/*
 * Enter the half Height of Diamond:3
  /\
 /  \
/    \
\    /
 \  /
  \/		*/
